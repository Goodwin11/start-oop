<?php

$objects = [];
foreach($products as $product){
    switch($product){
        case $product['type'] === 'hotel_room':
            $objects[] = new HotelRoom (
                $product['title'],
                $product['type'],
                $product['address'],
                $product['price'],
                $product['description'],
                $product['roomNumber']
            );
        break;
        case $product['type'] === 'apartment':
            $objects[] = new Apartment (
                $product['title'],
                $product['type'],
                $product['address'],
                $product['price'],
                $product['description'],
                $product['kitchen']
            );
        break;
        case $product['type'] === 'house':
            $objects[] = new HotelRoom (
                $product['title'],
                $product['type'],
                $product['address'],
                $product['price'],
                $product['description'],
                $product['roomsAmount']
            );
        break;
    }
};

?>