<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/data/products.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/Product.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/House.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/HotelRoom.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/Apartment.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/data/objects.php';

class HtmlWriter{
    public function write(Product $object){
       include $_SERVER['DOCUMENT_ROOT'].'/template/templates.php';
    }
};
$writer = new HtmlWriter();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <title>MyDay</title>
</head>
<body> <!--ссылка на 'show more'-->
    <?php foreach($objects as $date) : ?>
        <?php if($_GET['title'] === $date->title): ?>
            <?=$writer->write($date); ?>
        <?php endif; ?>
    <?php endforeach; ?>
</body>
</html>