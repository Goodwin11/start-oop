<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/data/products.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/Product.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/House.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/HotelRoom.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/Apartment.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/data/objects.php';
// выводы обектов "тип", "название", "цена"
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="Description" content="OOP is awesome if you now what I mean!">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <title>MyDay</title>
</head>
<body>
    <h1 class="text-center">World real estate market</h1>
    <hr>
    <?php foreach($objects as $date): ?>
        <div class="col-3">
            <div class="list-group list-group-flush">
                <h5 class="list-group-item"><?=$date->title?></h5>
                <p class="list-group-item"><?=$date->type;?></p>
                <p class="list-group-item"><?=$date->price?></p>
                <a href="htmlwriter.php?title=<?=$date->title; ?>" class="btn btn-info">Show More</a>
            </div>
        </div>
    <?php endforeach; ?>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</body>
</html>